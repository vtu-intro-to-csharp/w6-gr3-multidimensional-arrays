﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SumUnderMainDiagonal
{
    public class Program
    {
        //21.	Дадена е квадратна реална матрица.
        //Да се състави програма, която намира сумата на елементите под главния диагонал.
        public static void Main(string[] args)
        {
            double[,] matrix = InputSquareMatrix();

            double sum = 0;
            for(int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < i; j++)
                {
                    sum += matrix[i, j];
                }
            }

            Console.WriteLine($"The sum of the elements under the main diagonal: {sum}");
        }

        static double[,] InputSquareMatrix()
        {
            Console.WriteLine("Enter the dimensions of the matrix.");
            Console.Write("Rows/Columns = ");
            int dimension = int.Parse(Console.ReadLine());

            double[,] matrix = new double[dimension, dimension];

            for (int i = 0; i < dimension; i++)
            {
                for (int j = 0; j < dimension; j++)
                {
                    Console.Write($"Enter matrix[{i}, {j}] = ");
                    matrix[i, j] = double.Parse(Console.ReadLine());
                }
            }

            return matrix;
        }

    }
}
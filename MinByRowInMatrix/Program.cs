﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MinByRowInMatrix
{
    public class Program
    {
        //17.	Да се състави програма,
        //която намира и извежда минималния елемент на всеки ред на двумерен масив.
        public static void Main(string[] args)
        {
            int[,] matrix = InputMatrix();
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                int min = matrix[i, 0];
                for (int j = 1; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] < min)
                    {
                        min = matrix[i, j];
                    }
                }
                Console.WriteLine($"Min at Row {i + 1} = {min}");
            }
        }

        static int[,] InputMatrix()
        {
            Console.WriteLine("Enter the dimensions of the matrix.");
            Console.Write("Rows = ");
            int rows = int.Parse(Console.ReadLine());

            Console.Write("Columns = ");
            int columns = int.Parse(Console.ReadLine());

            int[,] matrix = new int[rows, columns];

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    Console.Write($"Enter matrix[{i}, {j}] = ");
                    matrix[i, j] = int.Parse(Console.ReadLine());
                }
            }

            return matrix;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FindX
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int[,] matrix = InputMatrix();
            Console.Write("Enter a number to search for: ");
            int x = int.Parse(Console.ReadLine());

            if (FindNegative(matrix))
            {
                Console.WriteLine("There is a negative element.");
            }
            else
            {
                Console.WriteLine("There is not a negative element.");
            }
        }

        static int[,] InputMatrix()
        {
            Console.WriteLine("Enter the dimensions of the matrix.");
            Console.Write("Rows = ");
            int rows = int.Parse(Console.ReadLine());

            Console.Write("Columns = ");
            int columns = int.Parse(Console.ReadLine());

            int[,] matrix = new int[rows, columns];

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    Console.Write($"Enter matrix[{i}, {j}] = ");
                    matrix[i, j] = int.Parse(Console.ReadLine());
                }
            }

            return matrix;
        }

        static bool FindNegative(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] < 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}